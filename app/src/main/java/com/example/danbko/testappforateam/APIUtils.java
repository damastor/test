package com.example.danbko.testappforateam;



public class APIUtils {

    private APIUtils() {}

    public static final String BASE_URL = "http://jsonplaceholder.typicode.com/";

    public static APIServicePosts getAPIPostsService() {

        return RetrofitClient.getClient(BASE_URL).create(APIServicePosts.class);
    }

    public static APIServicePhoto getAPIPhotoService() {

        return RetrofitClient.getClient(BASE_URL).create(APIServicePhoto.class);
    }

    public static APIServiceComments getAPICommentsService() {

        return RetrofitClient.getClient(BASE_URL).create(APIServiceComments.class);
    }
    public static APIServiceUsers getAPIUsersService() {

        return RetrofitClient.getClient(BASE_URL).create(APIServiceUsers.class);
    }
    public static APIServiceTodos getAPITodosService() {

        return RetrofitClient.getClient(BASE_URL).create(APIServiceTodos.class);
    }
}