package com.example.danbko.testappforateam;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;



public class PageFragment extends Fragment {
    private static final String ARG_TEXT = "arg_text";
    private static final String ARG_COLOR = "arg_color";

    private List<Card> cards;
    private int fragIndex;


    public static Fragment newInstance(int index) {
        Fragment fragment = new PageFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (getArguments().getInt("index", 0) == 0) {
            View view = inflater.inflate(R.layout.fragment_rv, container, false);
            initRV(view);
            return view;
        } else {
            View view = inflater.inflate(R.layout.fragment_contacts, container, false);
            initContacts(view);
            return view;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null) {
            Bundle args = getArguments();
            fragIndex = args.getInt("index");
        } else {
            fragIndex = savedInstanceState.getInt("index");
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("index", fragIndex);
        super.onSaveInstanceState(outState);
    }



    private void initRV(View view) {

        RecyclerView rv  = view.findViewById(R.id.my_recycler_view);
        LinearLayout llt = view.findViewById(R.id.llt);

        rv.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(llt.getContext());
        rv.setLayoutManager(llm);

        initializeData();

        RVAdapter adapter = new RVAdapter(cards);
        rv.setAdapter(adapter);
    }

    private void initContacts(View view){
//        String url = "https://pp.userapi.com/c627629/v627629728/47a30/xRKgKf04RPk.jpg";
        ImageView myPhoto = (ImageView)view.findViewById(R.id.profile_photo);
        Picasso.with(view.getContext()).load("https://pp.userapi.com/c627629/v627629728/47a30/xRKgKf04RPk.jpg").into(myPhoto);
    }

    private void initializeData(){
        cards = new ArrayList<>();
        cards.add(new Card("Posts",    R.color.colorPosts));
        cards.add(new Card("Comments", R.color.colorComments));
        cards.add(new Card("Photo",    R.color.colorPhoto));
        cards.add(new Card("Users",    R.color.colorUsers));
        cards.add(new Card("Todos",    R.color.colorTodos));
    }

    }