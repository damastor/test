package com.example.danbko.testappforateam;

import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.TypedValue;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;



public class ResponseManager {


    public static void sendMessage(CardView cardView){

        switch (cardView.getId()){
            case R.id.posts_cv:
                sendPost(cardView);
                break;

            case R.id.comments_cv:
                sendComment(cardView);
                break;

            case R.id.photo_cv:
                sendPhoto(cardView);
                break;

            case R.id.users_cv:
                sendUser(cardView, 0);
                break;

            case R.id.todos_cv:
                sendTodo(cardView);
                break;
        }
    }

    private static void sendPost(final CardView cardView) {

        EditText editText = cardView.findViewById(R.id.posts_cv_edittext);
        String id = editText.getText().toString();

        if (id.equals("")) {
            return;
        }
        MainActivity.mAPIServicePosts.savePost(id).enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                if (response.isSuccessful()) {

                    AddPost(response, cardView);
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    private static void sendPhoto(final CardView cardView) {

        EditText editText = cardView.findViewById(R.id.photo_cv_edittext);
        String id = editText.getText().toString();

        if (id.equals("")) {
            return;
        }

        MainActivity.mAPIServicePhoto.savePhoto(id).enqueue(new Callback<Photo>() {
            @Override
            public void onResponse(Call<Photo> call, Response<Photo> response) {

                if(response.isSuccessful()) {
                    AddPhoto(response, cardView);
                }
            }

            @Override
            public void onFailure(Call<Photo> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    private static void sendUser(final CardView cardView, final int i) {
//        EditText editText = cardView.findViewById(R.id.users_cv_edittext);
//        final String id = editText.getText().toString();

        if(i > 4){
            return;
        }

        MainActivity.mAPIServiceUsers.saveUser(String.valueOf(i + 1)).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                AddUser(response, cardView, String.valueOf(i + 1), i);
                sendUser(cardView, i + 1);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {


            }
        });

    }

    private static void sendComment(final CardView cardView) {
        EditText editText = cardView.findViewById(R.id.comments_cv_edittext);
        String id = editText.getText().toString();

        if (id.equals("")) {
            return;
        }
        MainActivity.mAPIServiceComments.saveComment(id).enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {

                if(response.isSuccessful()) {
                    AddComment(response, cardView);
                }
            }

            @Override
            public void onFailure(Call<Comment> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    private static void sendTodo(final CardView cardView) {
//        EditText editText = (EditText) cardView.findViewById(R.id.todos_cv_edittext);
//        String id = editText.getText().toString();
        Random rand = new Random();
        int rnd = rand.nextInt(200) + 1;
        String id = String.valueOf(rnd);

        MainActivity.mAPIServiceTodos.saveTodo(id).enqueue(new Callback<Todo>() {
            @Override
            public void onResponse(Call<Todo> call, Response<Todo> response) {

                if(response.isSuccessful()) {
                    AddTodo(response, cardView);
                }
            }

            @Override
            public void onFailure(Call<Todo> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }


    private static void AddPost(Response<Post> response, CardView cv){

        TextView textView = cv.findViewById(R.id.posts_cv_title);
        int pL = textView.getPaddingLeft();
        int pT = 0;
        int pB = textView.getPaddingBottom();

        LinearLayout llt = cv.findViewById(R.id.llt);
        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        String id    = "ID: "    + response.body().getId().toString();
        String title = "Title: " + response.body().getTitle();
        String body  = "Body: "  + response.body().getBody();

        if(cv.findViewById(R.id.post_id) == null){

            TextView idTextView = new TextView(cv.getContext());
            idTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            idTextView.setText(id);
            idTextView.setId(R.id.post_id);
            idTextView.setPadding(pL, pT, pL, pB);
            llt.addView(idTextView, tvParams);

            TextView titleTextView = new TextView(cv.getContext());
            titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            titleTextView.setText(title);
            titleTextView.setId(R.id.post_title);
            titleTextView.setPadding(pL, pT, pL, pB);
            llt.addView(titleTextView, tvParams);

            TextView bodyTextView = new TextView(cv.getContext());
            bodyTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            bodyTextView.setText(body);
            bodyTextView.setId(R.id.post_body);
            bodyTextView.setPadding(pL, pT, pL, pL);
            llt.addView(bodyTextView, tvParams);

        }
        else {
            TextView idTextView    = cv.findViewById(R.id.post_id);
            TextView titleTextView = cv.findViewById(R.id.post_title);
            TextView bodyTextView  = cv.findViewById(R.id.post_body);
            idTextView.setText(id);
            titleTextView.setText(title);
            bodyTextView.setText(body);
        }

    }

    private static void AddPhoto(Response<Photo> response, CardView cv) {

        TextView textView = cv.findViewById(R.id.photo_cv_title);
        int pL = textView.getPaddingLeft();
        int pT = 0;
        int pB = textView.getPaddingBottom();

        LinearLayout llt = cv.findViewById(R.id.llt);
        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        String title = "Title: " + response.body().getTitle();

        if(cv.findViewById(R.id.photo_image_title) == null) {

            TextView titleTextView = new TextView(cv.getContext());
            titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            titleTextView.setText(title);
            titleTextView.setId(R.id.photo_image_title);
            titleTextView.setPadding(pL, pT, pL, pB);
            llt.addView(titleTextView, tvParams);

            ImageView imageView = new ImageView(cv.getContext());
            imageView.setId(R.id.photo_image);
            imageView.setPadding(pL, pT, pL, pL);
            llt.addView(imageView, tvParams);

            Picasso.with(cv.getContext()).load(response.body().getUrl()).into((ImageView) cv.findViewById(R.id.photo_image));
        }
        else{
            TextView titleTextView = cv.findViewById(R.id.photo_image_title);
            ImageView imageView    = cv.findViewById(R.id.photo_image);
            titleTextView.setText(title);

            Picasso.with(cv.getContext()).load(response.body().getUrl()).into(imageView);
        }

    }

    private static void AddComment(Response<Comment> response, CardView cv){

        TextView textView = cv.findViewById(R.id.comments_cv_title);
        int pL = textView.getPaddingLeft();
        int pT = 0;
        int pB = textView.getPaddingBottom();

        LinearLayout llt = cv.findViewById(R.id.llt);
        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        String postId    = "Post ID: "    + response.body().getPostId().toString();
        String commentId = "Comment ID: " + response.body().getId().toString();
        String name      = "Name: "       + response.body().getName();
        String email     = "Email: "      + response.body().getEmail();
        String body      = "Body: "       + response.body().getBody();



        if(cv.findViewById(R.id.comment_id) == null){

            TextView postIdTextView = new TextView(cv.getContext());
            postIdTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            postIdTextView.setText(postId);
            postIdTextView.setId(R.id.comment_post_id);
            postIdTextView.setPadding(pL, pT, pL, pB);
            llt.addView(postIdTextView, tvParams);

            TextView idTextView = new TextView(cv.getContext());
            idTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            idTextView.setText(commentId);
            idTextView.setId(R.id.comment_id);
            idTextView.setPadding(pL, pT, pL, pB);
            llt.addView(idTextView, tvParams);

            TextView nameTextView = new TextView(cv.getContext());
            nameTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            nameTextView.setText(name);
            nameTextView.setId(R.id.comment_name);
            nameTextView.setPadding(pL, pT, pL, pB);
            llt.addView(nameTextView, tvParams);

            TextView emailTextView = new TextView(cv.getContext());
            emailTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            emailTextView.setText(email);
            emailTextView.setId(R.id.comment_email);
            emailTextView.setPadding(pL, pT, pL, pB);
            llt.addView(emailTextView, tvParams);

            TextView bodyTextView = new TextView(cv.getContext());
            bodyTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            bodyTextView.setText(body);
            bodyTextView.setId(R.id.comment_body);
            bodyTextView.setPadding(pL, pT, pL, pL);
            llt.addView(bodyTextView, tvParams);

        }
        else {
            TextView idTextView     = cv.findViewById(R.id.comment_post_id);
            TextView postIdTextView = cv.findViewById(R.id.comment_id);
            TextView nameTextView   = cv.findViewById(R.id.comment_name);
            TextView emailTextView  = cv.findViewById(R.id.comment_email);
            TextView bodyTextView   = cv.findViewById(R.id.comment_body);
            postIdTextView.setText(postId);
            idTextView.setText(commentId);
            nameTextView.setText(name);
            emailTextView.setText(email);
            bodyTextView.setText(body);
        }

    }

    private static void AddUser(Response<User> response, CardView cv, String id, int position){


        TextView textView = cv.findViewById(R.id.users_cv_title);
        int pL = textView.getPaddingLeft();
        int pT = 0;
        int pB = textView.getPaddingBottom();

        LinearLayout llt = cv.findViewById(R.id.llt);
        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        String userString = response.body().getName();

        int RID = 0;
        switch (position) {
            case 0:
                RID = R.id.user_1;
                break;
            case 1:
                RID = R.id.user_2;
                break;
            case 2:
                RID = R.id.user_3;
                break;
            case 3:
                RID = R.id.user_4;
                break;
            case 4:
                RID = R.id.user_5;
                break;
        }

        if(Integer.parseInt(id) + position > 10){
            if(cv.findViewById(RID) != null){
                TextView userTextView = cv.findViewById(RID);
                ((LinearLayout) userTextView.getParent()).removeView(userTextView);
            }
            return;
        }
        if(cv.findViewById(RID) == null){

            TextView userTextView = new TextView(cv.getContext());
            userTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            userTextView.setText(userString);
            userTextView.setId(RID);
            if(position == 4){
                userTextView.setPadding(pL, pT, pL, pL);
            }
            else {
                userTextView.setPadding(pL, pT, pL, pB);
            }
            llt.addView(userTextView, tvParams);

        }
        else {
            TextView userTextView = cv.findViewById(RID);
            userTextView.setText(userString);
        }

    }

    private static void AddTodo(Response<Todo> response, CardView cv){

        TextView textView = cv.findViewById(R.id.todos_cv_title);
        int pL = textView.getPaddingLeft();
        int pT = 0;
        int pB = textView.getPaddingBottom();


        LinearLayout llt = cv.findViewById(R.id.llt);
        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        String id        = "ID: "             + response.body().getId().toString();
        String userID    = "User ID: "        + response.body().getUserId().toString();
        String title     = "Title: "          + response.body().getTitle();
        String completed = "Todo completed: " + response.body().getCompleted().toString();

        if(cv.findViewById(R.id.todo_id) == null){

            TextView idTextView = new TextView(cv.getContext());
            idTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            idTextView.setText(id);
            idTextView.setId(R.id.todo_id);
            idTextView.setPadding(pL, pT, pL, pB);
            llt.addView(idTextView, tvParams);

            TextView userIdTextView = new TextView(cv.getContext());
            userIdTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            userIdTextView.setText(userID);
            userIdTextView.setId(R.id.todo_user_id);
            userIdTextView.setPadding(pL, pT, pL, pB);
            llt.addView(userIdTextView, tvParams);

            TextView titleTextView = new TextView(cv.getContext());
            titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            titleTextView.setText(title);
            titleTextView.setId(R.id.todo_title);
            titleTextView.setPadding(pL, pT, pL, pB);
            llt.addView(titleTextView, tvParams);

            TextView completedTextView = new TextView(cv.getContext());
            completedTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            completedTextView.setText(completed);
            completedTextView.setId(R.id.todo_completed);
            completedTextView.setPadding(pL, pT, pL, pL);
            llt.addView(completedTextView, tvParams);

        }
        else {
            TextView idTextView        = cv.findViewById(R.id.todo_id);
            TextView userIdTextView    = cv.findViewById(R.id.todo_user_id);
            TextView titleTextView     = cv.findViewById(R.id.todo_title);
            TextView completedTextView = cv.findViewById(R.id.todo_completed);
            idTextView.setText(id);
            userIdTextView.setText(userID);
            titleTextView.setText(title);
            completedTextView.setText(completed);
        }

    }

    private static void AddErrorMsg(int id, CardView cv){

    }
}
