package com.example.danbko.testappforateam;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface APIServiceTodos {

    @GET("/todos/{id}")
//    @FormUrlEncoded
    Call<Todo> saveTodo(@Path("id") String id);//
//    Call<Post> savePost(@Field("title") String title,
//                        @Field("body") String body,
//                        @Field("userId") long userId);
}