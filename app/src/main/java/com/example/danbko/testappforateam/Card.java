package com.example.danbko.testappforateam;

import android.graphics.drawable.ColorDrawable;

import java.util.ArrayList;
import java.util.List;



public class Card {
    String title;
    Integer color;

    Card(String title, Integer color) {
        this.title = title;
        this.color = color;
    }
}