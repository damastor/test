package com.example.danbko.testappforateam;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.List;


public class RVAdapter extends RecyclerView.Adapter<RVAdapter.CardViewHolder> {
    private List<Card> cards;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class CardViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView cardTitle;
        Button cardBtn;
        EditText editText;

        CardViewHolder(View itemView) {
            super(itemView);
            cv        = (CardView) itemView.findViewById(R.id.card_view);
            cardTitle = (TextView) itemView.findViewById(R.id.layout_item_title);
            cardBtn   = (Button)   itemView.findViewById(R.id.button2);
            editText  = (EditText) itemView.findViewById(R.id.editText);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RVAdapter(List<Card> cards){
        this.cards = cards;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_item, viewGroup, false);
        CardViewHolder cardViewHolder = new CardViewHolder(v);

        return cardViewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CardViewHolder cardViewHolder, int i) {
        String title = cards.get(i).title;
        cardViewHolder.cardTitle.setText(cards.get(i).title);
//        cardViewHolder.cv.setCardBackgroundColor(ContextCompat.getColor(cardViewHolder.cv.getContext(), cards.get(i).color));
        cardViewHolder.cardTitle.setBackgroundResource(cards.get(i).color);

        switch (title)
        {
            case "Posts":
                cardViewHolder.cv.setId       (R.id.posts_cv);
                cardViewHolder.cardTitle.setId(R.id.posts_cv_title);
                cardViewHolder.editText.setId (R.id.posts_cv_edittext);
                cardViewHolder.cardBtn.setId  (R.id.posts_cv_btn);
                break;

            case "Comments":
                cardViewHolder.cv.setId       (R.id.comments_cv);
                cardViewHolder.cardTitle.setId(R.id.comments_cv_title);
                cardViewHolder.editText.setId (R.id.comments_cv_edittext);
                cardViewHolder.cardBtn.setId  (R.id.comments_cv_btn);
                break;

            case "Photo":
                cardViewHolder.cv.setId       (R.id.photo_cv);
                cardViewHolder.cardTitle.setId(R.id.photo_cv_title);
                cardViewHolder.editText.setId (R.id.photo_cv_edittext);
                cardViewHolder.cardBtn.setId  (R.id.photo_cv_btn);
                break;

            case "Users":
                cardViewHolder.cv.setId       (R.id.users_cv);
                cardViewHolder.cardTitle.setId(R.id.users_cv_title);
                cardViewHolder.editText.setId (R.id.users_cv_edittext);
                cardViewHolder.cardBtn.setId  (R.id.users_cv_btn);
                if(cardViewHolder.cv.findViewById(R.id.users_cv_edittext)!=null) {
                    ((LinearLayout) cardViewHolder.editText.getParent()).removeView(cardViewHolder.editText);
                    cardViewHolder.cardBtn.setText("Показать 5 пользователей");
                }
                break;

            case "Todos":
                cardViewHolder.cv.setId       (R.id.todos_cv);
                cardViewHolder.cardTitle.setId(R.id.todos_cv_title);
                cardViewHolder.editText.setId (R.id.todos_cv_edittext);
                cardViewHolder.cardBtn.setId  (R.id.todos_cv_btn);
                if(cardViewHolder.cv.findViewById(R.id.todos_cv_edittext)!=null){
                    ((LinearLayout) cardViewHolder.editText.getParent()).removeView(cardViewHolder.editText);
                    cardViewHolder.cardBtn.setText("Случайная задача");
                }
                break;
        }
        cardViewHolder.cardBtn.setTag(cardViewHolder.cv);

//        cardViewHolder.editText.setText(cardViewHolder.cardBtn.getResources().getResourceName(cardViewHolder.cardBtn.getId()));

        cardViewHolder.cardBtn.setOnClickListener (new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // action on click
                CardView cardView = (CardView) v.getTag();

                switch (v.getId()) {
                    case R.id.posts_cv_btn:
                        ResponseManager.sendMessage(cardView);
                        break;

                    case R.id.comments_cv_btn:
                        ResponseManager.sendMessage(cardView);
                        break;

                    case R.id.photo_cv_btn:;
                        ResponseManager.sendMessage(cardView);
                        break;

                    case R.id.users_cv_btn:
                        ResponseManager.sendMessage(cardView);
                        break;

                    case R.id.todos_cv_btn:
                        ResponseManager.sendMessage(cardView);
                        break;
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return cards.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}

