package com.example.danbko.testappforateam;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface APIServiceUsers {

    @GET("/users/{id}")
//    @FormUrlEncoded
    Call<User> saveUser(@Path("id") String id);//
//    Call<Post> savePost(@Field("title") String title,
//                        @Field("body") String body,
//                        @Field("userId") long userId);
}