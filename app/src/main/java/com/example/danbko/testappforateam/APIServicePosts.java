package com.example.danbko.testappforateam;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIServicePosts {

    @GET("/posts/{id}")
//    @FormUrlEncoded
    Call<Post> savePost(@Path("id") String id);//
//    Call<Post> savePost(@Field("title") String title,
//                        @Field("body") String body,
//                        @Field("userId") long userId);
}